package com.example.xukaijun.avertallergy.activities;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.MainActivity;
import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.helpers.HttpUrlHelper;
import com.example.xukaijun.avertallergy.helpers.MyRecyclerViewAdapter;
import com.example.xukaijun.avertallergy.helpers.SwipeToDeleteCallback1;
import com.example.xukaijun.avertallergy.model.Food;
import com.example.xukaijun.avertallergy.model.Food1;
import com.example.xukaijun.avertallergy.model.FoodDatabase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.example.xukaijun.avertallergy.sql.RestClient;

public class HistoryFragment extends Fragment implements MyRecyclerViewAdapter.ItemClickListener {
    FoodDatabase db2 = null;
    private MyRecyclerViewAdapter adapter=null;
    private static final String EXTRA_CODE = "com.example.testingcodereading.code";
    private Button appCompatButtonClear;
    private Food food1=new Food();
    private ArrayList<String> foodInfo = new ArrayList<>();
    private ArrayList<Food> food_list = new ArrayList<>();
    private RecyclerView recyclerView=null;
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
    private Food1 food11 = new Food1();
    View vMain;

    public static HistoryFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CODE, code);

        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * This method is to initialize views
     */
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle
            savedInstanceState) {
        super.onCreate(savedInstanceState);
        db2 = Room.databaseBuilder((MainActivity)getContext(),
                FoodDatabase.class, "FoodDatabase") .fallbackToDestructiveMigration()
                .build();
        HttpUrlHelper httpUrlHelper = new HttpUrlHelper();
        vMain = inflater.inflate(R.layout.fragment_history, container, false);
        getActivity().setTitle("Item History");
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
        appCompatButtonClear= vMain.findViewById(R.id.appCompatButtonClear);
        appCompatButtonClear.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
                builder.setTitle("Clear History?" ) ;
                builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ClearDatabase clearDatabase = new ClearDatabase();
                        clearDatabase.execute();
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        return vMain;
    }

    public void setupRecyclerView(View view){
        // set up the RecyclerView
        Food temp=new Food();
        if (food_list.size()>1) {
            for (int p = 0; p < food_list.size() - 1; p++) {
                for (int q = p + 1; q < food_list.size(); q++) {
                    String time1 = food_list.get(p).getSdate();
                    String time2 = food_list.get(q).getSdate();
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");//change date format
                    Date date1 = null;
                    try {
                        date1 = format.parse(time1);
                        Date date2 = format.parse(time2);

                        if (date1.before(date2)) {
                            temp = food_list.get(p);
                            food_list.set(p,food_list.get(q));
                            food_list.set(q,temp);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        if (food_list.isEmpty()){
            foodInfo.add("No Record");
        }else {
            for (Food temp1:food_list){
                foodInfo.add(temp1.getSdate()+" \t"+temp1.getProduct_name());
            }
        }
        recyclerView = vMain.findViewById(R.id.food_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MyRecyclerViewAdapter(getContext(), foodInfo);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        appCompatButtonClear.setVisibility(View.VISIBLE);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback1(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    //read local database
    private class ReadDatabase extends AsyncTask<Void, Void, Void> {
        @Override
    protected Void doInBackground(Void... params) {
        List<Food> foodList = db2.foodDao().getAll();
        if (!foodList.isEmpty())
        {
            for (Food temp : foodList) {
                food_list.add(temp);
            }
        }else {
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void strings) {
            setupRecyclerView(vMain);
        }
    }


    @Override
    public void onItemClick(View view, int position) {
        String[] info = adapter.getItem(position).split(" ");
        try{
            for (Food temp: food_list){
                if (temp.getSdate().equals(info[0]+" "+info[1]))
                {
                    food1= temp;
                }
            }
            long code = Long.parseLong(food1.getCode());
            Query query = reference.child("0").orderByKey().equalTo(String.valueOf(code));
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        // dataSnapshot is the "issue" node with all children with id 0
                        for (DataSnapshot issue : dataSnapshot.getChildren()) {
                            food11 = issue.getValue(Food1.class);
                        }
                        if (!food11.ingredients_text.equals("")){
                            food1.setIngredients_text(food11.ingredients_text);}
                    }else {
                        food11.setCode(Long.parseLong(food1.getCode()));
                        food11.setNegative(0);
                        food11.setPositive(0);
                        food11.setProduct_name(food1.getProduct_name());
                        food11.setIngredients_text(food1.getIngredients_text());
                        food11.setImage_url(food1.getImage_url());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            final AlertDialog.Builder builder1  = new AlertDialog.Builder(getContext());
            builder1.setTitle("Your feedback is important for us and other users") ;
            builder1.setMessage("Do you think the item information provided is accurate?");
            builder1.setPositiveButton("Accurate",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    writepositive();
                    dialog.dismiss();
                    Toast.makeText(getContext(),"Thank you for your feedback!",Toast.LENGTH_SHORT).show();


                }
            });
            builder1.setNegativeButton("Not Accurate",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    writenegative();
                    dialog.dismiss();
                    Toast.makeText(getContext(),"Thank you for your feedback!",Toast.LENGTH_SHORT).show();

                }
            });
            AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
            builder.setTitle(food1.getStatus() ) ;
            builder.setMessage(food1.getIngredients_text() ) ;
            builder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Give Feedback",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    builder1.show();
                    dialog.dismiss();
                }
            });
            builder.show();
        }catch (Exception e){}
    }

    private class ClearDatabase extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
           db2.foodDao().deleteAll();
           return "Successful";
        }

        @Override
        protected void onPostExecute(String user1) {
            foodInfo = new ArrayList<>();
            foodInfo.add("No Record");
            adapter = new MyRecyclerViewAdapter(getContext(), foodInfo);
            recyclerView.setAdapter(adapter);
            }
        }

    private void writepositive() {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = reference.child("0").child(String.valueOf(food11.code)).push().getKey();
        food11.setPositive(food11.positive+1);
        Map<String, Object> postValues = food11.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/0/" + String.valueOf(food11.code), postValues);
        reference.updateChildren(childUpdates);
    }
    private void writenegative() {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = reference.child("0").child(String.valueOf(food11.code)).push().getKey();
        food11.setNegative(food11.negative+1);
        Map<String, Object> postValues = food11.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/0/" + String.valueOf(food11.code), postValues);
        reference.updateChildren(childUpdates);
    }
}
