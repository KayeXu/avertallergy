package com.example.xukaijun.avertallergy.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.MainActivity;
import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.helpers.CaptureActivityPortrait;
import com.example.xukaijun.avertallergy.helpers.HttpUrlHelper;
import com.example.xukaijun.avertallergy.model.Allergy;
import com.example.xukaijun.avertallergy.model.AllergyDatabase;
import com.example.xukaijun.avertallergy.model.Food;
import com.example.xukaijun.avertallergy.model.Food1;
import com.example.xukaijun.avertallergy.model.FoodDatabase;
import com.example.xukaijun.avertallergy.model.User;
import com.example.xukaijun.avertallergy.model.UserDatabase;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

//import com.example.xukaijun.avertallergy.sql.RestClient;

public class MainFragment extends Fragment {
    UserDatabase db = null;
    AllergyDatabase db1 = null;
    FoodDatabase db2 = null;
    FoodDatabase db4 = null;
    FoodDatabase db5 = null;
    private ArrayList<Allergy> allergyArrayList = new ArrayList<>();
    private TextView textView_FoodName, textView_Suggestion, textView_Suggestion1, textView_barcode,textname;
    private static final String EXTRA_CODE = "com.example.testingcodereading.code";
    private AppCompatImageView appCompatImageView_food, appCompatImageView_emoji;
    private Button appCompatButtonHome, appCompatButtonIngredient,appCompatButtonFeedback,appCompatButtonback;
    private ImageButton imageButton_scanner;
    private int id;
    private String url;
    private Food food1 = new Food();
    private TextView mTextMessage;
    private ProgressDialog progressDialog;
    private LinearLayout linearLayout,linearLayout1,linearLayout2,float1;
    private FloatingActionButton fab, fab1, fab2;
    private Animation fab_open, fab_close, fab_clock, fab_anticlock;
    TextView textview_shop, textview_forbid;
    Boolean isOpen = false;
    private ArrayList<Food> food_list = new ArrayList<>();
    private Food1 food11 = new Food1();
    private PieChart pieChart;
    View vMain;

    public static MainFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CODE, code);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * This method is to initialize views
     */
    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle
            savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = Room.databaseBuilder((MainActivity) getContext(),
                UserDatabase.class, "UserDatabase").fallbackToDestructiveMigration()
                .build();
        db1 = Room.databaseBuilder((MainActivity) getContext(),
                AllergyDatabase.class, "AllergyDatabase").fallbackToDestructiveMigration()
                .build();
        db2 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "FoodDatabase").fallbackToDestructiveMigration()
                .build();
        db4 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ShopList").fallbackToDestructiveMigration()
                .build();
        db5 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ForbidList").fallbackToDestructiveMigration()
                .build();
        HttpUrlHelper httpUrlHelper = new HttpUrlHelper();
        id = ((MainActivity) getActivity()).getId();
        vMain = inflater.inflate(R.layout.fragment_main, container, false);
        getActivity().setTitle("Home");
        textView_FoodName = vMain.findViewById(R.id.textView_FoodName);
        textView_Suggestion = vMain.findViewById(R.id.textView_Suggestion);
        textView_Suggestion1 = vMain.findViewById(R.id.textView_Suggestion1);
        textView_barcode = vMain.findViewById(R.id.textView_barcode);
        textname = vMain.findViewById(R.id.textname);
        appCompatImageView_food = vMain.findViewById(R.id.AppCompatImageView_food);
        appCompatImageView_emoji = vMain.findViewById(R.id.AppCompatImageView_emoji);
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
        appCompatButtonIngredient = vMain.findViewById(R.id.appCompatButtonIngredient);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("loading...");
        appCompatButtonHome = vMain.findViewById(R.id.appCompatButtonhome);
        imageButton_scanner = vMain.findViewById(R.id.imageButton_scanner);
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）
        int hight = dm.heightPixels;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(imageButton_scanner.getLayoutParams());
        lp.setMargins((int) (width * 7 / 24), hight * 3 / 12, (int) 0, 0);
        lp.width = (int) (width / 3);
        lp.height = (int) (width / 3);
        //imageButton_scanner.setLayoutParams(lp);
        linearLayout = vMain.findViewById(R.id.choose_list);
        linearLayout1 = vMain.findViewById(R.id.resultpage);
        linearLayout2 = vMain.findViewById(R.id.piepage);
        float1 = vMain.findViewById(R.id.float1);
        pieChart = vMain.findViewById(R.id.pie_chart);
        imageButton_scanner.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                scanNow(v);
            }
        });
        appCompatButtonIngredient.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                appCompatButtonHome.setVisibility(View.VISIBLE);
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Ingredient");
                builder.setMessage(food1.getIngredients_text());
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        appCompatButtonHome.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Home");
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });
        appCompatButtonback = vMain.findViewById(R.id.appCompatButtonback);
        appCompatButtonFeedback = vMain.findViewById(R.id.appCompatButtonFeedback);
        appCompatButtonFeedback.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                    getActivity().setTitle("Feedback");
                textname.setText(food1.getProduct_name());
                linearLayout2.setVisibility(View.VISIBLE);
                linearLayout1.setVisibility(View.GONE);
                try {
                    generatePieData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pieChart.getDescription().setEnabled(false);
                pieChart.setCenterText(generateCenterText());
                pieChart.setCenterTextSize(10f);
                // radius of the center hole in percent of maximum radius
                pieChart.setHoleRadius(45f);
                pieChart.setTransparentCircleRadius(50f);
                pieChart.getLegend().setEnabled(true);
                pieChart.animateX(1000);
                Legend legend = pieChart.getLegend();
                List<LegendEntry> legends = new ArrayList<>();
                LegendEntry blue = new LegendEntry();
                blue.label = "Accurate";
                blue.formColor = Color.parseColor("#B2FF66");
                legends.add(blue);
                LegendEntry red = new LegendEntry();
                red.label = "Not Accurate";
                red.formColor = Color.RED;
                legends.add(red);
                pieChart.getLegend().setCustom(legends);
            }
        });
        appCompatButtonback.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Results");
                linearLayout1.setVisibility(View.VISIBLE);
                linearLayout2.setVisibility(View.GONE);
            }
        });
        fab = vMain.findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab1 = vMain.findViewById(R.id.fab1);
        fab2 = vMain.findViewById(R.id.fab2);
        fab_close = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
        fab_open = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fab_clock = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_clock);
        fab_anticlock = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_anticlock);

        textview_shop = (TextView) vMain.findViewById(R.id.textview_shop);
        textview_forbid = (TextView) vMain.findViewById(R.id.textview_forbid);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOpen) {

                    textview_shop.setVisibility(View.INVISIBLE);
                    textview_forbid.setVisibility(View.INVISIBLE);
                    fab2.startAnimation(fab_close);
                    fab1.startAnimation(fab_close);
                    fab.startAnimation(fab_anticlock);
                    fab2.setClickable(false);
                    fab1.setClickable(false);
                    isOpen = false;
                } else {
                    textview_shop.setVisibility(View.VISIBLE);
                    textview_forbid.setVisibility(View.VISIBLE);
                    fab2.startAnimation(fab_open);
                    fab1.startAnimation(fab_open);
                    fab.startAnimation(fab_clock);
                    fab2.setClickable(true);
                    fab1.setClickable(true);
                    isOpen = true;
                }

            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview_shop.setVisibility(View.INVISIBLE);
                textview_forbid.setVisibility(View.INVISIBLE);
                fab2.startAnimation(fab_close);
                fab1.startAnimation(fab_close);
                fab.startAnimation(fab_anticlock);
                fab2.setClickable(false);
                fab1.setClickable(false);
                isOpen = false;
                try {
                    Food food = new Food();
                    food.setCode(food1.getProduct_name());
                    if (food1.getProduct_name().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Failure");
                        builder.setMessage("Product not exist!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                    } else {
                        UpdateDatabase1 updateDatabase1 = new UpdateDatabase1();
                        updateDatabase1.execute(food).get();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Update");
                        builder.setMessage("Success!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        builder.show();

                    }
                }  catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Failure");
                    builder.setMessage("Product not exist!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            }

        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview_shop.setVisibility(View.INVISIBLE);
                textview_forbid.setVisibility(View.INVISIBLE);
                fab2.startAnimation(fab_close);
                fab1.startAnimation(fab_close);
                fab.startAnimation(fab_anticlock);
                fab2.setClickable(false);
                fab1.setClickable(false);
                isOpen = false;
                UpdateDatabase2 updateDatabase2 = new UpdateDatabase2();
                try {
                    Boolean judge;
                    judge = updateDatabase2.execute(food1).get();
                    if (judge) {
                        textview_forbid.setText("Remove from Forbid List");
                        textView_Suggestion1.setVisibility(View.VISIBLE);
                        textView_Suggestion1.setText("This item in your forbid List!");
                        textView_Suggestion1.setTextColor(Color.parseColor("#FF0000"));
                    } else {
                        textview_forbid.setText("Add to Forbid List");
                        textView_Suggestion1.setVisibility(View.GONE);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Update");
                    builder.setMessage("Success!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        return vMain;
    }

    //scan method
    @SuppressLint("RestrictedApi")
    public void scanNow(View view) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
        } else {
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.forSupportFragment(MainFragment.this).setPrompt("Place the barcode perpendicular to the red line.").setCameraId(0)
                    .setBarcodeImageEnabled(true).setOrientationLocked(true).setBeepEnabled(true).setCaptureActivity(CaptureActivityPortrait.class).initiateScan();
        }
        getActivity().setTitle("Results");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator integrator = new IntentIntegrator(getActivity());
                    integrator.forSupportFragment(MainFragment.this).setPrompt("Place the barcode perpendicular to the red line.").initiateScan();
                } else {
                    Toast.makeText(getContext(), "Camera can't open", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }

    //scan result return
    @SuppressLint("RestrictedApi")
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {
            fab.setVisibility(View.VISIBLE);
            imageButton_scanner.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            float1.setVisibility(View.VISIBLE);
            super.onActivityResult(requestCode, resultCode, intent);
            System.out.println("never here");
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanningResult != null) {
                String scanContent = scanningResult.getContents();
                String scanFormat = scanningResult.getFormatName();
                textView_barcode.setText(scanContent);
                appCompatImageView_food.setVisibility(View.GONE);
                appCompatButtonIngredient.setVisibility(View.GONE);
                textView_Suggestion.setVisibility(View.GONE);
                textView_barcode.setVisibility(View.INVISIBLE);
                if (textView_barcode.getText().equals("")) {
                    startActivity(new Intent(getContext(), LoginActivity.class));
                } else {
                    food1.setCode(textView_barcode.getText().toString());
                    url = "https://world.openfoodfacts.org/api/v0/product/" + textView_barcode.getText() + ".json";
                    progressDialog.show();
                    SearchCode searchCode = new SearchCode();
                    searchCode.execute(url);
                }
            } else {
                Toast toast = Toast.makeText(getContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (Exception e) {
        }

    }

    //read local database
    private class ReadDatabase extends AsyncTask<Void, Void, ArrayList<String>> {
        @Override
        protected ArrayList doInBackground(Void... params) {
            List<User> users = db.userDao().getAll();
            User user = new User();
            List<Allergy> allergies = db1.allergyDao().getAll();
            List<Food> foodList = db2.foodDao().getAll();
            if (!foodList.isEmpty()) {
                for (Food temp : foodList) {
                    food_list.add(temp);
                }
            } else {
            }
            Allergy allergy = new Allergy();
            user = users.get(0);
            ArrayList<String> strings = new ArrayList<>();
            strings.add(0, user.getFirstName());
            allergyArrayList = new ArrayList<>();
            for (Allergy temp : allergies) {
                allergyArrayList.add(temp);
            }
            return strings;
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            String name = strings.get(0);
            strings.remove(0);
            String temp = "";
            for (String i : strings) {
                temp += i;
                temp += " ";
            }
            //textInputLayoutGoalstep.setHint(String.valueOf(user1.getGoalsteps()));
        }
    }

    //search the code on open food fact
    public class SearchCode extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String test = new HttpUrlHelper().getHttpUrlConnection(params[0]);
            return test;
        }

        @Override
        protected void onPostExecute(String mix) {
            textView_FoodName.setText(mix);
            try {
                JSONObject job = new JSONObject(mix);
                Food food = new Food();
                textView_Suggestion.setVisibility(View.VISIBLE);
                if (job.getJSONObject("product").has("product_name_en")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name_en"));
                } else if (job.getJSONObject("product").has("product_name")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name"));
                } else if (job.getJSONObject("product").has("product_name_fr")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name_fr"));
                } else {
                    food.setProduct_name("None");
                }
                if (job.getJSONObject("product").has("brands")) {
                    food.setBrands(job.getJSONObject("product").getString("brands"));
                } else {
                    food.setBrands(" ");
                }
                if (job.getJSONObject("product").has("ingredients_text_en"))
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_text_en"));
                else if (job.getJSONObject("product").has("ingredients_text")) {
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_text"));
                } else if (job.getJSONObject("product").has("ingredients_tags")) {
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_tags"));
                } else {
                    food.setIngredients_text("none");
                }
                if (!Pattern.compile("(?i)[a-z]").matcher(food.getIngredients_text()).find()) {//contain character is true
                    food.setIngredients_text("none");
                }
                boolean judge = false;
                String test1 = "";
                String test2 = "";
                if (!allergyArrayList.isEmpty() && !food.getIngredients_text().equals("none")) {
                    for (Allergy allergy : allergyArrayList) {
                        if (!test2.equals("")) {
                            test2 += " and ";
                        }
                        test2 += allergy.getAllergyName();
                        if (food.getIngredients_text().toLowerCase().indexOf(allergy.getAllergyName()) != -1) {
                            if (!test1.equals("")) {
                                test1 += " and ";
                            }
                            test1 += allergy.getAllergyName();
                            judge = true;
                        }
                    }
                }
                food1.setProduct_name(food.getProduct_name());
                food1.setIngredients_text(food.getIngredients_text());
                if (job.getJSONObject("product").has("image_url")) {
                    food.setImage_url(job.getJSONObject("product").getString("image_url"));
                } else if (job.getJSONObject("product").has("image_small_url")) {
                    food.setImage_url(job.getJSONObject("product").getString("image_small_url"));
                }
                try {
                    ImageTask imageTask = new ImageTask();
                    imageTask.execute(food.getImage_url());
                } catch (Exception e) {
                }
                long code = Long.parseLong(food1.getCode());
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("0").orderByKey().equalTo(String.valueOf(code));
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                food11 = issue.getValue(Food1.class);
                            }
                            if (!food11.ingredients_text.equals("")){
                                food1.setIngredients_text(food11.ingredients_text);}
                            appCompatButtonFeedback.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                textView_FoodName.setText(food.getProduct_name() + " " + food.getBrands());
                textView_FoodName.setVisibility(View.VISIBLE);
                appCompatButtonHome.setVisibility(View.VISIBLE);
                if (food.getProduct_name().length() > 100 || food.getIngredients_text().equals("none") || food.getProduct_name().equals("none")) {
                    textView_Suggestion.setText("Sorry, this item is not in our database or relevant information is not fully recorded yet");
                    food1.setStatus("Not fully recorded yet");
                    textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                    appCompatButtonHome.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji3);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);
                } else if (judge) {
                    textView_Suggestion.setText("The published product information says this product contains " + test1);
                    food1.setStatus("This product contains " + test1);
                    textView_Suggestion.setTextColor(Color.parseColor("#FF0000"));
                    textView_FoodName.setText(food.getProduct_name());
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji2);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);

                } else {
                    textView_Suggestion.setText("The published product information says this product is " + test2 + " free");
                    food1.setStatus("This product is " + test2 + " free");
                    textView_FoodName.setText(food.getProduct_name());
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji1);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                textView_Suggestion.setText("Sorry, this item is not in our database or relevant information is not fully recorded yet");
                food1.setStatus("Not fully recorded yet");
                textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                appCompatButtonHome.setVisibility(View.VISIBLE);
                appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji3);
                appCompatImageView_emoji.setVisibility(View.VISIBLE);
            }
            UpdateDatabase updateDatabase = new UpdateDatabase();
            updateDatabase.execute(food1);
            ReadDatabase1 readDatabase1 = new ReadDatabase1();
            try {
                Boolean judge;
                judge = readDatabase1.execute(food1).get();
                if (judge) {
                    textview_forbid.setText("Add to Forbid List");
                    textView_Suggestion1.setVisibility(View.GONE);
                } else {
                    textview_forbid.setText("Remove from Forbid List");
                    textView_Suggestion1.setVisibility(View.VISIBLE);
                    textView_Suggestion1.setText("This item in your forbid List!");
                    textView_Suggestion1.setTextColor(Color.parseColor("#FF0000"));
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 500);
        }
    }

    //get image
    private class ImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... keyword) {
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(keyword[0]).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            appCompatImageView_food.setVisibility(View.VISIBLE);
            appCompatImageView_food.setImageBitmap(result);
        }
    }

    private class UpdateDatabase extends AsyncTask<Food, Void, Void> {
        @Override
        protected Void doInBackground(Food... params) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            String scanDate = sdf.format(new Date());
            params[0].setSdate(scanDate);
            boolean judge = true;
            for (Food temp : db2.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    db2.foodDao().updateFoods(params[0]);
                    judge = false;
                }
            }
            if (judge) {
                db2.foodDao().insert(params[0]);
            }
            return null;
        }
    }

    private class UpdateDatabase1 extends AsyncTask<Food, Void, Void> {
        @Override
        protected Void doInBackground(Food... params) {
            try {
                boolean judge = true;
                for (Food temp : db4.foodDao().getAll()) {
                    if (temp.getCode().equals(params[0].getCode())) {
                        db4.foodDao().updateFoods(params[0]);
                        judge = false;
                    }
                }
                if (judge) {
                    db4.foodDao().insert(params[0]);
                }
                return null;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private class UpdateDatabase2 extends AsyncTask<Food, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db5.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    db5.foodDao().delete(params[0]);
                    judge = false;
                }
            }
            if (judge) {
                db5.foodDao().insert(params[0]);
            }
            return judge;
        }
    }

    private class ReadDatabase1 extends AsyncTask<Food, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db5.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    judge = false;
                }
            }
            return judge;
        }
    }
    private SpannableString generateCenterText() {
        SpannableString s = new SpannableString("Accuracy\n "+ food11.product_name);
        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
        return s;
    }

    private PieData generatePieData() throws JSONException {
        double positive = food11.positive;
        double negative = food11.negative;
        ArrayList<PieEntry> entries1 = new ArrayList<>();
        entries1.add(new PieEntry((float) positive+2));
        entries1.add(new PieEntry((float) negative+1));
        PieDataSet ds1 = new PieDataSet(entries1, "Positive Negative");
        final int[] piecolors = new int[]{
                Color.parseColor("#B2FF66"),
                Color.rgb(255, 0, 0)};
        ds1.setColors(ColorTemplate.createColors(piecolors));
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.BLACK);
        ds1.setValueTextSize(12f);
        PieData pieData = new PieData(ds1);
        pieChart.setData(pieData);
        return pieData;
    }
}
