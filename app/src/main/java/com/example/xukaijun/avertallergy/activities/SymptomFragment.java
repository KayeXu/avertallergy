package com.example.xukaijun.avertallergy.activities;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.model.Symptom;
import com.example.xukaijun.avertallergy.model.SymptomDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SymptomFragment extends Fragment {
    SymptomDatabase db3 = null;
    CheckBox symptom1, symptom2, symptom3, symptom4, symptom5, symptom6, symptom7, symptom8, symptom9, symptom10, symptom11,symptom12;
    private TextInputLayout textInputLayoutComment;
    private TextInputEditText textInputEditTextComment;
    private AppCompatButton appCompatButtonSubmit;
    private Spinner spinner;
    View vMain;

    /**
     * This method is to initialize views
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        vMain = inflater.inflate(R.layout.fragment_symptom, container, false);
        getActivity().setTitle("Log New Symptom");
        textInputLayoutComment = (TextInputLayout) vMain.findViewById(R.id.textInputLayoutComment);
        textInputEditTextComment = (TextInputEditText) vMain.findViewById(R.id.textInputEditTextComment);
        spinner = (Spinner) vMain.findViewById(R.id.levelofsevere);
    // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.level_array, android.R.layout.simple_spinner_item);
    // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        symptom1 = (CheckBox) vMain.findViewById(R.id.symptom1);
        symptom2 = (CheckBox) vMain.findViewById(R.id.symptom2);
        symptom3 = (CheckBox) vMain.findViewById(R.id.symptom3);
        symptom4 = (CheckBox) vMain.findViewById(R.id.symptom4);
        symptom5 = (CheckBox) vMain.findViewById(R.id.symptom5);
        symptom6 = (CheckBox) vMain.findViewById(R.id.symptom6);
        symptom7 = (CheckBox) vMain.findViewById(R.id.symptom7);
        symptom8 = (CheckBox) vMain.findViewById(R.id.symptom8);
        symptom9 = (CheckBox) vMain.findViewById(R.id.symptom9);
        symptom10 = (CheckBox) vMain.findViewById(R.id.symptom10);
        symptom11 = (CheckBox) vMain.findViewById(R.id.symptom11);
        symptom12 = (CheckBox) vMain.findViewById(R.id.symptom12);
        db3 = Room.databaseBuilder(getContext(),
                SymptomDatabase.class, "SymptomDatabase").fallbackToDestructiveMigration()
                .build();
        Button appCompatButtonSubmit = (Button) vMain.findViewById(R.id.appCompatButtonSubmit);
        appCompatButtonSubmit.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                String temp1 = "";
                if (symptom1.isChecked()) {
                    temp1 += symptom1.getText() + "\n";
                }
                if (symptom2.isChecked()) {
                    temp1 += symptom2.getText() + "\n";
                }
                if (symptom3.isChecked()) {
                    temp1 += symptom3.getText() + "\n";
                }
                if (symptom4.isChecked()) {
                    temp1 += symptom4.getText() + "\n";
                }
                if (symptom5.isChecked()) {
                    temp1 += symptom5.getText() + "\n";
                }
                if (symptom6.isChecked()) {
                    temp1 += symptom6.getText() + "\n";
                }
                if (symptom7.isChecked()) {
                    temp1 += symptom7.getText() + "\n";
                }
                if (symptom8.isChecked()) {
                    temp1 += symptom8.getText() + "\n";
                }
                if (symptom9.isChecked()) {
                    temp1 += symptom9.getText() + "\n";
                }
                if (symptom10.isChecked()) {
                    temp1 += symptom10.getText() + "\n";
                }
                if (symptom11.isChecked()) {
                    temp1 += symptom11.getText() + "\n";
                }
                if (symptom12.isChecked()) {
                    temp1 += symptom12.getText() + "\n";
                }
                if (temp1.equals("")){
                    Toast.makeText(getContext(), "Please select at least one symptom!",
                            Toast.LENGTH_SHORT).show();
                }else {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String submitDate = sdf.format(new Date());
                UpdateDatabase updateDatabase = new UpdateDatabase();
                updateDatabase.execute(temp1,submitDate,textInputEditTextComment.getText().toString(),spinner.getSelectedItem().toString());
            }}
        });
        return vMain;
    }

    //update to local database
    private class UpdateDatabase extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            Symptom symptom = new Symptom();
            symptom.setSymptom_name(params[0]);
            symptom.setSdate(params[1]);
            symptom.setComment(params[2]);
            symptom.setLevel(params[3]);
            db3.symptomDao().insert(symptom);
            return "Success";
        }

        @Override
        protected void onPostExecute(String message) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Update");
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                }
            });
            builder.show();
        }
    }

}
