package com.example.xukaijun.avertallergy.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.SearchView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.MainActivity;
import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.helpers.HttpUrlHelper;
import com.example.xukaijun.avertallergy.model.Allergy;
import com.example.xukaijun.avertallergy.model.AllergyDatabase;
import com.example.xukaijun.avertallergy.model.Food;
import com.example.xukaijun.avertallergy.model.Food1;
import com.example.xukaijun.avertallergy.model.FoodDatabase;
import com.example.xukaijun.avertallergy.model.User;
import com.example.xukaijun.avertallergy.model.UserDatabase;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import static android.support.constraint.Constraints.TAG;

public class SearchFragment extends Fragment {
    UserDatabase db = null;
    AllergyDatabase db1 = null;
    FoodDatabase db2 = null;
    FoodDatabase db4 = null;
    FoodDatabase db5 = null;
    View searchView;
    private TextView TextView_description, textView_Selected;
    private AppCompatImageView appCompatImageView_food, appCompatImageView_emoji;
    private TextView textView_FoodName, textView_Suggestion, textView_Suggestion1,textname;
    private List<HashMap<String, String>> foodArray, foodSearchArray;
    private ListView foodList;
    private SimpleAdapter foodSearchAdapter;
    private String[] foodSearchHEAD;
    private int[] foodSearchCELL;
    private String[] foodSearchHEAD1;
    private int[] foodSearchCELL1;
    private Button appCompatButtonHome, appCompatButtonIngredient,appCompatButtonFeedback,appCompatButtonback;
    private TextInputEditText textInputEditTextSearch, textInputEditTextServe, textInputEditTextCategory;
    private ArrayList<Food> food_list = new ArrayList<>();
    private ArrayList<Allergy> allergyArrayList = new ArrayList<>();
    private SearchView searchview;
    private Food food1 = new Food();
    private LinearLayout linearLayout, linearLayout1,linearLayout2;
    private ProgressDialog progressDialog;
    private FloatingActionButton fab, fab1, fab2;
    private Animation fab_open, fab_close, fab_clock, fab_anticlock;
    private TextView textview_shop, textview_forbid;
    private Food1 food11 = new Food1();
    private PieChart pieChart;

    Boolean isOpen = false;

    @SuppressLint("RestrictedApi")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db = Room.databaseBuilder((MainActivity) getContext(),
                UserDatabase.class, "UserDatabase").fallbackToDestructiveMigration()
                .build();
        db1 = Room.databaseBuilder((MainActivity) getContext(),
                AllergyDatabase.class, "AllergyDatabase").fallbackToDestructiveMigration()
                .build();
        db2 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "FoodDatabase").fallbackToDestructiveMigration()
                .build();
        db4 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ShopList").fallbackToDestructiveMigration()
                .build();
        db5 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ForbidList").fallbackToDestructiveMigration()
                .build();
        searchView = inflater.inflate(R.layout.fragment_search, container, false);
        getActivity().setTitle("Search Item");
        linearLayout = searchView.findViewById(R.id.choose_list);
        linearLayout1 = searchView.findViewById(R.id.resultpage);
        linearLayout2 = searchView.findViewById(R.id.piepage);
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
        HashMap<String, String> map = new HashMap<String, String>();
        appCompatImageView_food = searchView.findViewById(R.id.AppCompatImageView_food);
        appCompatImageView_emoji = searchView.findViewById(R.id.AppCompatImageView_emoji);
        textView_FoodName = searchView.findViewById(R.id.textView_FoodName);
        textView_Suggestion = searchView.findViewById(R.id.textView_Suggestion);
        textView_Suggestion1 = searchView.findViewById(R.id.textView_Suggestion1);
        textname = searchView.findViewById(R.id.textname);
        foodSearchHEAD = new String[]{"FOOD INFO"};
        foodSearchCELL = new int[]{R.id.foodbarcode};
        foodSearchHEAD = new String[]{"FOOD INFO1"};
        foodSearchCELL = new int[]{R.id.foodname};
        foodList = searchView.findViewById(R.id.food_list);
        foodList.setVerticalScrollBarEnabled(true);
        foodList.setScrollbarFadingEnabled(true);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("loading...");
        pieChart = searchView.findViewById(R.id.pie_chart);
        foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //GetFoodAsyncTask getFoodAsyncTask = new GetFoodAsyncTask();
                String category = "";
                HashMap<String, String> map = (HashMap<String, String>) foodList.getItemAtPosition(position);
                String foodInfo = map.get("FOOD INFO");
                food1.setCode(foodInfo);
                String url = "https://world.openfoodfacts.org/api/v0/product/" + food1.getCode() + ".json";
                progressDialog.show();
                SearchCode searchCode = new SearchCode();
                searchCode.execute(url);
                linearLayout.setVisibility(View.GONE);
                linearLayout1.setVisibility(View.VISIBLE);
                linearLayout2.setVisibility(View.GONE);
                fab.setVisibility(View.VISIBLE);
                long code = food11.code;
            }
        });
        searchview = (SearchView) searchView.findViewById(R.id.searchview);
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) {
                    linearLayout.setVisibility(View.VISIBLE);
                    linearLayout1.setVisibility(View.GONE);
                    linearLayout2.setVisibility(View.GONE);
                    foodList.setVisibility(View.VISIBLE);
                    SearchFatAsyncTask searchFatAsyncTask = new SearchFatAsyncTask();
                    searchFatAsyncTask.execute("https://world.openfoodfacts.org/cgi/search.pl?search_terms=" + query + "&search_simple=1&action=process&countries=Australia&json=1");
                } else {
                    foodList.setVisibility(View.GONE);
                }
                return false;
            }

            //搜索框内部改变回调，newText就是搜索框里的内容
            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.isEmpty()) {
                    linearLayout.setVisibility(View.VISIBLE);
                    linearLayout1.setVisibility(View.GONE);
                    foodList.setVisibility(View.VISIBLE);
                    SearchFatAsyncTask searchFatAsyncTask = new SearchFatAsyncTask();
                    searchFatAsyncTask.execute("https://world.openfoodfacts.org/cgi/search.pl?search_terms=" + newText + "&search_simple=1&action=process&countries=Australia&json=1");
                } else {
                    foodList.setVisibility(View.GONE);
                }
                return false;
            }
        });
        appCompatButtonHome = searchView.findViewById(R.id.appCompatButtonhome);
        appCompatButtonback = searchView.findViewById(R.id.appCompatButtonback);
        appCompatButtonIngredient = searchView.findViewById(R.id.appCompatButtonIngredient);
        appCompatButtonIngredient.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                appCompatButtonHome.setVisibility(View.VISIBLE);
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Ingredient");
                builder.setMessage(food1.getIngredients_text());
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        appCompatButtonFeedback = searchView.findViewById(R.id.appCompatButtonFeedback);
        appCompatButtonFeedback.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                    getActivity().setTitle("Feedback");
                    textname.setText(food1.getProduct_name());
                linearLayout2.setVisibility(View.VISIBLE);
                linearLayout1.setVisibility(View.GONE);
                try {
                    generatePieData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pieChart.getDescription().setEnabled(false);
                pieChart.setCenterText(generateCenterText());
                pieChart.setCenterTextSize(10f);
                // radius of the center hole in percent of maximum radius
                pieChart.setHoleRadius(45f);
                pieChart.setTransparentCircleRadius(50f);
                pieChart.getLegend().setEnabled(true);
                pieChart.animateX(1000);
                Legend legend = pieChart.getLegend();
                List<LegendEntry> legends = new ArrayList<>();
                LegendEntry blue = new LegendEntry();
                blue.label = "Accurate";
                blue.formColor = Color.parseColor("#B2FF66");
                legends.add(blue);
                LegendEntry red = new LegendEntry();
                red.label = "Not Accurate";
                red.formColor = Color.RED;
                legends.add(red);
                pieChart.getLegend().setCustom(legends);
            }
        });
        appCompatButtonHome.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Search Item");
                linearLayout1.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
        appCompatButtonback.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Results");
                linearLayout1.setVisibility(View.VISIBLE);
                linearLayout2.setVisibility(View.GONE);
            }
        });
        fab = searchView.findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab1 = searchView.findViewById(R.id.fab1);
        fab2 = searchView.findViewById(R.id.fab2);
        fab_close = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
        fab_open = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fab_clock = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_clock);
        fab_anticlock = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_anticlock);

        textview_shop = (TextView) searchView.findViewById(R.id.textview_shop);
        textview_forbid = (TextView) searchView.findViewById(R.id.textview_forbid);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOpen) {

                    textview_shop.setVisibility(View.INVISIBLE);
                    textview_forbid.setVisibility(View.INVISIBLE);
                    fab2.startAnimation(fab_close);
                    fab1.startAnimation(fab_close);
                    fab.startAnimation(fab_anticlock);
                    fab2.setClickable(false);
                    fab1.setClickable(false);
                    isOpen = false;
                } else {
                    textview_shop.setVisibility(View.VISIBLE);
                    textview_forbid.setVisibility(View.VISIBLE);
                    fab2.startAnimation(fab_open);
                    fab1.startAnimation(fab_open);
                    fab.startAnimation(fab_clock);
                    fab2.setClickable(true);
                    fab1.setClickable(true);
                    isOpen = true;
                }

            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview_shop.setVisibility(View.INVISIBLE);
                textview_forbid.setVisibility(View.INVISIBLE);
                fab2.startAnimation(fab_close);
                fab1.startAnimation(fab_close);
                fab.startAnimation(fab_anticlock);
                fab2.setClickable(false);
                fab1.setClickable(false);
                isOpen = false;
                Food food = new Food();
                food.setCode(food1.getProduct_name());
                UpdateDatabase1 updateDatabase1 = new UpdateDatabase1();
                try {
                    updateDatabase1.execute(food).get();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Update");
                    builder.setMessage("Success!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textview_shop.setVisibility(View.INVISIBLE);
                textview_forbid.setVisibility(View.INVISIBLE);
                fab2.startAnimation(fab_close);
                fab1.startAnimation(fab_close);
                fab.startAnimation(fab_anticlock);
                fab2.setClickable(false);
                fab1.setClickable(false);
                isOpen = false;
                UpdateDatabase2 updateDatabase2 = new UpdateDatabase2();
                try {
                    Boolean judge;
                    judge = updateDatabase2.execute(food1).get();
                    if (judge) {
                        textview_forbid.setText("Remove from Forbid List");
                        textView_Suggestion1.setVisibility(View.VISIBLE);
                        textView_Suggestion1.setText("This item in your forbid List!");
                        textView_Suggestion1.setTextColor(Color.parseColor("#FF0000"));
                    } else {
                        textview_forbid.setText("Add to Forbid List");
                        textView_Suggestion1.setVisibility(View.GONE);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Update");
                    builder.setMessage("Success!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        return searchView;
    }

    private class ReadDatabase extends AsyncTask<Void, Void, ArrayList<String>> {
        @Override
        protected ArrayList doInBackground(Void... params) {
            List<User> users = db.userDao().getAll();
            User user = new User();
            List<Allergy> allergies = db1.allergyDao().getAll();
            List<Food> foodList = db2.foodDao().getAll();
            if (!foodList.isEmpty()) {
                for (Food temp : foodList) {
                    food_list.add(temp);
                }
            } else {
            }
            Allergy allergy = new Allergy();
            user = users.get(0);
            ArrayList<String> strings = new ArrayList<>();
            strings.add(0, user.getFirstName());
            allergyArrayList = new ArrayList<>();
            for (Allergy temp : allergies) {
                allergyArrayList.add(temp);
            }
            return strings;
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            String name = strings.get(0);
            strings.remove(0);
            String temp = "";
            for (String i : strings) {
                temp += i;
                temp += " ";
            }
            //textInputLayoutGoalstep.setHint(String.valueOf(user1.getGoalsteps()));
        }
    }

    private class SearchFatAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String test = new HttpUrlHelper().getHttpUrlConnection(params[0]);
            return test;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                foodSearchArray = new ArrayList<HashMap<String, String>>();
                List<String> allFoodList = new ArrayList<String>();
                JSONObject temp = new JSONObject(result);
                JSONArray foodsJArray = temp.getJSONArray("products");
                for (int i = 0; i < foodsJArray.length(); i++) {
                    JSONObject food1 = (JSONObject) foodsJArray.get(i);
                    Food food = new Food();
                    food.setCode(food1.getString("code"));
                    if (food1.has("product_name_en")) {
                        food.setProduct_name(food1.getString("product_name_en"));
                    } else if (food1.has("product_name")) {
                        food.setProduct_name(food1.getString("product_name"));
                    } else if (food1.has("product_name_fr")) {
                        food.setProduct_name(food1.getString("product_name_fr"));
                    } else {
                        food.setProduct_name("None");
                    }
                    if (food1.has("brands")) {
                        food.setBrands(food1.getString("brands"));
                    } else {
                        food.setBrands(" ");
                    }
                    String foodAllInfo = food.getCode() + "    " + food.getProduct_name() + " " + food.getBrands() + "    " + food.getImage_url();
                    allFoodList.add(foodAllInfo);
                }
                if (allFoodList.size()==0)
                {Toast toast = Toast.makeText(getContext(),
                        "No result!", Toast.LENGTH_SHORT);}
                for (String foodInfomation : allFoodList) {
                    String[] infoSegment = foodInfomation.split("    ");
                    HashMap<String, String> foodInfoMap = new HashMap<String, String>();
                    foodInfoMap.put("FOOD INFO", infoSegment[0]);
                    foodInfoMap.put("FOOD INFO1", infoSegment[1]);
                    foodSearchArray.add(foodInfoMap);
                }
                foodSearchAdapter = new SimpleAdapter(getActivity(), foodSearchArray, R.layout.foodlist1, new String[]{"FOOD INFO", "FOOD INFO1"}, new int[]{R.id.foodbarcode, R.id.foodname});
                foodList.setAdapter(foodSearchAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
        }
    }

    private class ImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... keyword) {
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(keyword[0]).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            appCompatImageView_food.setVisibility(View.VISIBLE);
            appCompatImageView_food.setImageBitmap(result);
        }
    }

    public class SearchCode extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String test = new HttpUrlHelper().getHttpUrlConnection(params[0]);
            return test;
        }

        @Override
        protected void onPostExecute(String mix) {
            textView_FoodName.setText(mix);
            try {
                JSONObject job = new JSONObject(mix);
                Food food = new Food();
                textView_Suggestion.setVisibility(View.VISIBLE);
                if (job.getJSONObject("product").has("product_name_en")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name_en"));
                } else if (job.getJSONObject("product").has("product_name")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name"));
                } else if (job.getJSONObject("product").has("product_name_fr")) {
                    food.setProduct_name(job.getJSONObject("product").getString("product_name_fr"));
                } else {
                    food.setProduct_name("None");
                }
                if (job.getJSONObject("product").has("brands")) {
                    food.setBrands(job.getJSONObject("product").getString("brands"));
                } else {
                    food.setBrands(" ");
                }
                if (job.getJSONObject("product").has("ingredients_text_en"))
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_text_en"));
                else if (job.getJSONObject("product").has("ingredients_text")) {
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_text"));
                } else if (job.getJSONObject("product").has("ingredients_tags")) {
                    food.setIngredients_text(job.getJSONObject("product").getString("ingredients_tags"));
                } else {
                    food.setIngredients_text("none");
                }
                if (!Pattern.compile("(?i)[a-z]").matcher(food.getIngredients_text()).find()) {//contain character is true
                    food.setIngredients_text("none");
                }
                boolean judge = false;
                String test1 = "";
                String test2 = "";
                if (!allergyArrayList.isEmpty() && !food.getIngredients_text().equals("none")) {
                    for (Allergy allergy : allergyArrayList) {
                        if (!test2.equals("")) {
                            test2 += " and ";
                        }
                        test2 += allergy.getAllergyName();
                        if (food.getIngredients_text().toLowerCase().indexOf(allergy.getAllergyName()) != -1) {
                            if (!test1.equals("")) {
                                test1 += " and ";
                            }
                            test1 += allergy.getAllergyName();
                            judge = true;
                        }
                    }
                }
                food1.setProduct_name(food.getProduct_name());
                food1.setIngredients_text(food.getIngredients_text());
                if (job.getJSONObject("product").has("image_url")) {
                    food.setImage_url(job.getJSONObject("product").getString("image_url"));
                    try {
                        ImageTask imageTask = new ImageTask();
                        imageTask.execute(food.getImage_url());
                    } catch (Exception e) {
                    }
                }
                long code = Long.parseLong(food1.getCode());
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("0").orderByKey().equalTo(String.valueOf(code));
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            // dataSnapshot is the "issue" node with all children with id 0
                            for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                food11 = issue.getValue(Food1.class);
                            }
                            if (!food11.ingredients_text.equals("")){
                            food1.setIngredients_text(food11.ingredients_text);}
                            appCompatButtonFeedback.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                textView_FoodName.setText(food.getProduct_name() + " " + food.getBrands());
                textView_FoodName.setVisibility(View.VISIBLE);
                appCompatButtonHome.setVisibility(View.VISIBLE);
                if (food.getProduct_name().length() > 100 || food.getIngredients_text().equals("none") || food.getProduct_name().equals("none")) {
                    textView_Suggestion.setText("Sorry, this item is not in our database or relevant information is not fully recorded yet");
                    food1.setStatus("Not fully recorded yet");
                    textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                    appCompatButtonHome.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji3);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);
                } else if (judge) {
                    textView_Suggestion.setText("The published product information says this product contains " + test1);
                    food1.setStatus("This product contains " + test1);
                    textView_Suggestion.setTextColor(Color.parseColor("#FF0000"));
                    textView_FoodName.setText(food.getProduct_name());
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji2);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);

                } else {
                    textView_Suggestion.setText("The published product information says this product is " + test2 + " free");
                    food1.setStatus("This product is " + test2 + " free");
                    textView_FoodName.setText(food.getProduct_name());
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                    appCompatButtonIngredient.setVisibility(View.VISIBLE);
                    appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji1);
                    appCompatImageView_emoji.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                textView_Suggestion.setText("Sorry, this item is not in our database or relevant information is not fully recorded yet");
                food1.setStatus("Not fully recorded yet");
                textView_Suggestion.setTextColor(Color.parseColor("#000000"));
                appCompatButtonHome.setVisibility(View.VISIBLE);
                appCompatImageView_emoji.setBackgroundResource(R.drawable.emoji3);
                appCompatImageView_emoji.setVisibility(View.VISIBLE);
            }
            UpdateDatabase updateDatabase = new UpdateDatabase();
            updateDatabase.execute(food1);
            ReadDatabase1 readDatabase1 = new ReadDatabase1();
            try {
                Boolean judge;
                judge = readDatabase1.execute(food1).get();
                if (judge) {
                    textview_forbid.setText("Add to Forbid List");
                    textView_Suggestion1.setVisibility(View.GONE);
                } else {
                    textview_forbid.setText("Remove from Forbid List");
                    textView_Suggestion1.setVisibility(View.VISIBLE);
                    textView_Suggestion1.setText("This item in your forbid List!");
                    textView_Suggestion1.setTextColor(Color.parseColor("#FF0000"));
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 500);
        }

        private class UpdateDatabase extends AsyncTask<Food, Void, Void> {
            @Override
            protected Void doInBackground(Food... params) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                String scanDate = sdf.format(new Date());
                params[0].setSdate(scanDate);
                boolean judge = true;
                for (Food temp : db2.foodDao().getAll()) {
                    if (temp.getCode().equals(params[0].getCode())) {
                        db2.foodDao().updateFoods(params[0]);
                        judge = false;
                    }
                }
                if (judge) {
                    db2.foodDao().insert(params[0]);
                }
                return null;
            }
        }
    }

    private class UpdateDatabase1 extends AsyncTask<Food, Void, Void> {
        @Override
        protected Void doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db4.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    db4.foodDao().updateFoods(params[0]);
                    judge = false;
                }
            }
            if (judge) {
                db4.foodDao().insert(params[0]);
            }
            return null;
        }
    }

    private class UpdateDatabase2 extends AsyncTask<Food, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db5.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    db5.foodDao().delete(params[0]);
                    judge = false;
                }
            }
            if (judge) {
                db5.foodDao().insert(params[0]);
            }
            return judge;
        }
    }

    private class ReadDatabase1 extends AsyncTask<Food, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db5.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    judge = false;
                }
            }
            return judge;
        }
    }

    private SpannableString generateCenterText() {
        SpannableString s = new SpannableString("Accuracy\n "+ food11.product_name);
        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
        return s;
    }

    private PieData generatePieData() throws JSONException {
        double positive = food11.positive;
        double negative = food11.negative;
        ArrayList<PieEntry> entries1 = new ArrayList<>();
        double total = positive+2 + negative+1;
        DecimalFormat df = new DecimalFormat( "0.0000 ");
        double posipercent = Double.parseDouble(df.format((positive+2)/total));
        double negapercent = Double.parseDouble(df.format((negative+1)/total));
        entries1.add(new PieEntry((float) (posipercent*100)));
        entries1.add(new PieEntry((float) (negapercent*100)));
        PieDataSet ds1 = new PieDataSet(entries1, "Positive Negative");
        final int[] piecolors = new int[]{
                Color.parseColor("#B2FF66"),
                Color.rgb(255, 0, 0)};
        ds1.setColors(ColorTemplate.createColors(piecolors));
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.BLACK);
        ds1.setValueTextSize(12f);
        PieData pieData = new PieData(ds1);
        pieData.setDrawValues(true);
        pieData.setValueFormatter(new PercentFormatter());
        pieChart.setData(pieData);
        return pieData;
    }
}
