package com.example.xukaijun.avertallergy.activities;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.xukaijun.avertallergy.MainActivity;
import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.helpers.HttpUrlHelper;
import com.example.xukaijun.avertallergy.helpers.MyRecyclerViewAdapter2;
import com.example.xukaijun.avertallergy.helpers.MyRecyclerViewAdapter3;
import com.example.xukaijun.avertallergy.helpers.SwipeToDeleteCallback2;
import com.example.xukaijun.avertallergy.helpers.SwipeToDeleteCallback3;
import com.example.xukaijun.avertallergy.model.Food;
import com.example.xukaijun.avertallergy.model.FoodDatabase;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

//import com.example.xukaijun.avertallergy.sql.RestClient;

public class ListFragment extends Fragment implements MyRecyclerViewAdapter2.ItemClickListener, MyRecyclerViewAdapter3.ItemClickListener {
    FoodDatabase db4 = null;
    FoodDatabase db5 = null;
    private MyRecyclerViewAdapter2 adapter = null;
    private MyRecyclerViewAdapter3 adapter3 = null;
    private static final String EXTRA_CODE = "com.example.testingcodereading.code";
    private Button appCompatButtonClear, appCompatButtonBack, appCompatButtonClear1, appCompatButtonBack1, appCompatButtonAdd;
    private ImageButton appCompatButtonShop, appCompatButtonForbid;
    private LinearLayout listchoosepage, shoplistpage, forbidlistpage;
    private TextInputLayout textInputLayoutAdd;
    private TextInputEditText textInputEditTextAdd;
    private Food food1 = new Food();
    private ArrayList<String> foodInfo = new ArrayList<>();
    private ArrayList<Food> food_list = new ArrayList<>();
    private RecyclerView recyclerView = null;
    private RecyclerView recyclerView3 = null;
    private ArrayList<String> foodInfo3 = new ArrayList<>();
    private ArrayList<Food> food_list3 = new ArrayList<>();
    View vMain;

    public static ListFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CODE, code);

        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * This method is to initialize views
     */
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle
            savedInstanceState) {
        super.onCreate(savedInstanceState);
        db4 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ShopList").fallbackToDestructiveMigration()
                .build();
        db5 = Room.databaseBuilder((MainActivity) getContext(),
                FoodDatabase.class, "ForbidList").fallbackToDestructiveMigration()
                .build();
        HttpUrlHelper httpUrlHelper = new HttpUrlHelper();
        vMain = inflater.inflate(R.layout.fragment_list, container, false);
        listchoosepage = vMain.findViewById(R.id.listchoosepage);
        shoplistpage = vMain.findViewById(R.id.shoplistpage);
        forbidlistpage = vMain.findViewById(R.id.forbidlistpage);
        textInputLayoutAdd = (TextInputLayout) vMain.findViewById(R.id.textInputLayoutAdd);
        textInputEditTextAdd = (TextInputEditText) vMain.findViewById(R.id.textInputEditTextAdd);
        getActivity().setTitle("Lists");
        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();
        appCompatButtonShop = vMain.findViewById(R.id.appCompatButtonShop);
        appCompatButtonShop.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                listchoosepage.setVisibility(View.GONE);
                shoplistpage.setVisibility(View.VISIBLE);
                getActivity().setTitle("Shopping List");
            }
        });
        appCompatButtonForbid = vMain.findViewById(R.id.appCompatButtonForbid);
        appCompatButtonForbid.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                listchoosepage.setVisibility(View.GONE);
                forbidlistpage.setVisibility(View.VISIBLE);
                getActivity().setTitle("Forbid List");
            }
        });
        appCompatButtonAdd = vMain.findViewById(R.id.appCompatButtonAdd);
        appCompatButtonAdd.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                UpdateDatabase updateDatabase = new UpdateDatabase();
                food1 = new Food();
                if (!textInputEditTextAdd.getText().toString().equals("")) {
                    food1.setCode(textInputEditTextAdd.getText().toString());
                    textInputEditTextAdd.setText("");
                    try {
                        Boolean judge;
                        judge = updateDatabase.execute(food1).get();
                        if (foodInfo.get(0).equals("No Record"))
                        {
                            foodInfo.remove(0);
                            adapter.notifyItemRemoved(0);
                            adapter.notifyItemChanged(foodInfo.size(),foodInfo.size());
                        }
                        if (judge == true) {
                            foodInfo.add(food1.getCode());
                            adapter.notifyItemInserted(foodInfo.size());
                            adapter.notifyItemRangeChanged(foodInfo.size(), foodInfo.size());
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Update");
                        builder.setMessage("Success!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Failure");
                    builder.setMessage("require product name!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            }
        });
        appCompatButtonClear = vMain.findViewById(R.id.appCompatButtonClear);
        appCompatButtonClear.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Clear History?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ClearDatabase clearDatabase = new ClearDatabase();
                        clearDatabase.execute();
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        appCompatButtonClear1 = vMain.findViewById(R.id.appCompatButtonClear1);
        appCompatButtonClear1.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Clear History?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ClearDatabase1 clearDatabase1 = new ClearDatabase1();
                        clearDatabase1.execute();
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        appCompatButtonBack = vMain.findViewById(R.id.appCompatButtonBack);
        appCompatButtonBack.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Lists");
                shoplistpage.setVisibility(View.GONE);
                listchoosepage.setVisibility(View.VISIBLE);
            }
        });
        appCompatButtonBack1 = vMain.findViewById(R.id.appCompatButtonBack1);
        appCompatButtonBack1.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                getActivity().setTitle("Lists");
                forbidlistpage.setVisibility(View.GONE);
                listchoosepage.setVisibility(View.VISIBLE);
            }
        });
        return vMain;
    }

    public void setupRecyclerView(View view) {
        // set up the RecyclerView
        Food temp = new Food();
        if (food_list.isEmpty()) {
            foodInfo.add("No Record");
        } else {
            foodInfo = new ArrayList<>();
            for (Food temp1 : food_list) {
                foodInfo.add(temp1.getCode());
            }
        }
        recyclerView = vMain.findViewById(R.id.shop_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MyRecyclerViewAdapter2(getContext(), foodInfo);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        appCompatButtonShop.setVisibility(View.VISIBLE);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback2(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    public void setupRecyclerView3(View view) {
        // set up the RecyclerView
        Food temp = new Food();
        if (food_list3.isEmpty()) {
            foodInfo3.add("No Record");
        } else {
            foodInfo3 = new ArrayList<>();
            for (Food temp1 : food_list3) {
                foodInfo3.add(temp1.getProduct_name());
            }
        }
        recyclerView3 = vMain.findViewById(R.id.forbid_list);
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter3 = new MyRecyclerViewAdapter3(getContext(), foodInfo3);
        adapter3.setClickListener(this);
        recyclerView3.setAdapter(adapter3);
        recyclerView3.setVisibility(View.VISIBLE);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback3(adapter3));
        itemTouchHelper.attachToRecyclerView(recyclerView3);

    }

    //read local database
    private class ReadDatabase extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            List<Food> foodList = db4.foodDao().getAll();
            food_list = new ArrayList<>();
            if (!foodList.isEmpty()) {
                for (Food temp : foodList) {
                    food_list.add(temp);
                }
            } else {
            }
            List<Food> foodList3 = db5.foodDao().getAll();
            food_list3 = new ArrayList<>();
            if (!foodList3.isEmpty()) {
                for (Food temp : foodList3) {
                    food_list3.add(temp);
                }
            } else {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void strings) {
            setupRecyclerView(vMain);
            setupRecyclerView3(vMain);
        }
    }


    @Override
    public void onItemClick(View view, int position) {
        String info = adapter3.getItem(position);
        try {
            for (Food temp : food_list3) {
                if (temp.getProduct_name().equals(info)) {
                    food1 = temp;
                }
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(food1.getStatus());
            builder.setMessage(food1.getIngredients_text());
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
            builder.show();
        } catch (Exception e) {
        }
    }

    private class ClearDatabase extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            db4.foodDao().deleteAll();
            return "Successful";
        }

        @Override
        protected void onPostExecute(String user1) {
            foodInfo = new ArrayList<>();
            foodInfo.add("No Record");
            adapter = new MyRecyclerViewAdapter2(getContext(), foodInfo);
            recyclerView.setAdapter(adapter);
        }
    }

    private class ClearDatabase1 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            db5.foodDao().deleteAll();
            return "Successful";
        }

        @Override
        protected void onPostExecute(String user1) {
            foodInfo3 = new ArrayList<>();
            foodInfo3.add("No Record");
            adapter3 = new MyRecyclerViewAdapter3(getContext(), foodInfo3);
            recyclerView3.setAdapter(adapter3);
        }
    }

    private class UpdateDatabase extends AsyncTask<Food, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Food... params) {
            boolean judge = true;
            for (Food temp : db4.foodDao().getAll()) {
                if (temp.getCode().equals(params[0].getCode())) {
                    db4.foodDao().updateFoods(params[0]);
                    judge = false;
                }
            }
            if (judge) {
                db4.foodDao().insert(params[0]);
            }
            return judge;
        }
    }
}
