package com.example.xukaijun.avertallergy.activities;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.R;
import com.example.xukaijun.avertallergy.helpers.CalendarList;
import com.example.xukaijun.avertallergy.helpers.MyAdapter;
import com.example.xukaijun.avertallergy.helpers.SwipeToDeleteCallback;
import com.example.xukaijun.avertallergy.model.Symptom;
import com.example.xukaijun.avertallergy.model.SymptomDatabase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

//import com.example.xukaijun.avertallergy.sql.RestClient;

public class ShistoryFragment extends Fragment implements MyAdapter.ItemClickListener {
    SymptomDatabase db3 = null;
    private MyAdapter adapter=null;
    private static final String EXTRA_CODE = "com.example.testingcodereading.code";
    private Button appCompatButtonClear;
    private ImageButton appCompatButtonCalendar;
    private TextView textViewDate;
    private Symptom symptom1=new Symptom();
    private ArrayList<String> symptomInfo = new ArrayList<>();
    private ArrayList<Symptom> symptom_list = new ArrayList<>();
    private RecyclerView recyclerView=null;
    private CalendarList calendarList = null;
    private LineChart chart = null;
    View vMain;

    public ShistoryFragment() {
    }

    public static ShistoryFragment newInstance(String code) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CODE, code);

        ShistoryFragment fragment = new ShistoryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static List<String> getBetweenDays(String stime, String etime) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        Date sdate = null;
        Date eDate = null;
        try {
            sdate = df.parse(stime);
            eDate = df.parse(etime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long betweendays = (long) ((eDate.getTime() - sdate.getTime()) / (1000 * 60 * 60 * 24) + 0.5);//天数间隔
        Calendar c = Calendar.getInstance();
        List<String> list = new ArrayList<String>();
        while (sdate.getTime() <= eDate.getTime()) {
            list.add(df.format(sdate));
            System.out.println(df.format(sdate));
            c.setTime(sdate);
            c.add(Calendar.DATE, 1); // 日期加1天
            sdate = c.getTime();
        }
        return list;
    }

    /**
     * This method is to initialize views
     */
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle
            savedInstanceState) {
        super.onCreate(savedInstanceState);
        db3 = Room.databaseBuilder(getContext(),
                SymptomDatabase.class, "SymptomDatabase").fallbackToDestructiveMigration()
                .build();
        vMain = inflater.inflate(R.layout.fragment_shistory, container, false);
        getActivity().setTitle("Symptom History");
        textViewDate = vMain.findViewById(R.id.textViewDate);
        ReadDatabase readDatabase = new ReadDatabase();
        try {
            readDatabase.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        chart = (LineChart) vMain.findViewById(R.id.chart);
        calendarList = vMain.findViewById(R.id.calendarList);
        calendarList.setOnDateSelected(new CalendarList.OnDateSelected() {
            @Override
            public void selected(String startDate, String endDate) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
                try {
                    Date date1 = new Date();
                    Date date2 = sdf.parse(endDate);
                    if (date1.compareTo(date2) < 0) {
                        Toast.makeText(getContext(), "End Date after Today, choose again", Toast.LENGTH_LONG).show();
                    } else {
                        List<String> datelist = new ArrayList<>();
                        List<String> resultList = new ArrayList<String>();
                        datelist = getBetweenDays(startDate, endDate);
                        YAxis leftAxis = chart.getAxisLeft();
                        List<Entry> yVals = new ArrayList<Entry>();
                        int count = 0;
                        for (String i : datelist) {
                            int num = 0;
                            for (Symptom symptom : symptom_list) {
                                String[] info = symptom.getSdate().split(" ");
                                if (info[0].equals(i)) {
                                    num += 1;
                                }
                            }
                            resultList.add(String.valueOf(num));
                            yVals.add(new Entry(count, Integer.parseInt(String.valueOf(num))));
                            count += 1;
                        }
                        LineDataSet dataSet = new LineDataSet(yVals, "Frequency of symptoms");
                        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
                        dataSet.setDrawValues(false);
                        LineData lineData = new LineData(dataSet);
                        chart.setData(lineData);
                        XAxis xAxisFromChart = chart.getXAxis();
                        xAxisFromChart.setValueFormatter(new IndexAxisValueFormatter(datelist));
                        xAxisFromChart.setDrawAxisLine(true);
                        // minimum axis-step (interval) is 1,if no, the same value will be displayed multiple times
                        xAxisFromChart.setGranularity(1f);
                        xAxisFromChart.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxisFromChart.setLabelRotationAngle(-45);
                        chart.setVisibleYRange(0, 10, YAxis.AxisDependency.LEFT);
                        textViewDate.setText(startDate + " to " + endDate);
                        calendarList.setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);
                        appCompatButtonCalendar.setVisibility(View.VISIBLE);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        appCompatButtonClear= vMain.findViewById(R.id.appCompatButtonClear);
        appCompatButtonClear.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
                builder.setTitle("Clear History?" ) ;
                builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        ClearDatabase clearDatabase = new ClearDatabase();
                        clearDatabase.execute();
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        appCompatButtonCalendar = vMain.findViewById(R.id.appCompatButtonCalendar);
        appCompatButtonCalendar.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                appCompatButtonCalendar.setVisibility(View.GONE);
                calendarList.setVisibility(View.VISIBLE);
                chart.setVisibility(View.GONE);
            }
        });
        setupRecyclerView(vMain);
        return vMain;
    }

    public void setupRecyclerView(View view){
        // set up the RecyclerView
        Symptom temp=new Symptom();
        if (symptom_list.size()>1) {
            for (int p = 0; p < symptom_list.size() - 1; p++) {
                for (int q = p + 1; q < symptom_list.size(); q++) {
                    String time1 = symptom_list.get(p).getSdate();
                    String time2 = symptom_list.get(q).getSdate();
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");//change date format
                    Date date1 = null;
                    try {
                        date1 = format.parse(time1);
                        Date date2 = format.parse(time2);

                        if (date1.before(date2)) {
                            temp = symptom_list.get(p);
                            symptom_list.set(p,symptom_list.get(q));
                            symptom_list.set(q,temp);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        if (symptom_list.isEmpty()){
            symptomInfo.add("No Record");
        }else {
            for (Symptom temp1:symptom_list){
                symptomInfo.add(temp1.getSdate() + " \t" + "|" + "    " + "level: " + temp1.getLevel() + "    " + "|" + " \t" + temp1.getComment());
            }
        }
        recyclerView = vMain.findViewById(R.id.symptom_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MyAdapter(getContext(), symptomInfo);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        appCompatButtonClear.setVisibility(View.VISIBLE);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    public void onItemClick(View view, int position) {
        String[] info = adapter.getItem(position).split(" ");
        try{
            for (Symptom temp: symptom_list){
                if (temp.getSdate().equals(info[0]+" "+info[1]))
                {
                    symptom1= temp;
                }
            }
            AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
            builder.setTitle("Symptoms" ) ;
            builder.setMessage(symptom1.getSymptom_name() ) ;
            builder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
            builder.show();
        }catch (Exception e){}
    }

    private class ClearDatabase extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
           db3.symptomDao().deleteAll();
           return "Successful";
        }

        @Override
        protected void onPostExecute(String user1) {
            symptomInfo = new ArrayList<>();
            symptomInfo.add("No Record");
            adapter = new MyAdapter(getContext(), symptomInfo);
            recyclerView.setAdapter(adapter);
            }
        }

    //read local database
    private class ReadDatabase extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            List<Symptom> symptomList = db3.symptomDao().getAll();
            if (!symptomList.isEmpty()) {
                for (Symptom temp : symptomList) {
                    symptom_list.add(temp);
                }
            } else {
            }
            return null;
        }
    }
}
