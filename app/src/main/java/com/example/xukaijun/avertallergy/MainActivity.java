package com.example.xukaijun.avertallergy;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xukaijun.avertallergy.activities.AboutUsFragment;
import com.example.xukaijun.avertallergy.activities.EditFragment;
import com.example.xukaijun.avertallergy.activities.HelpFragment;
import com.example.xukaijun.avertallergy.activities.HistoryFragment;
import com.example.xukaijun.avertallergy.activities.ListFragment;
import com.example.xukaijun.avertallergy.activities.LoginActivity;
import com.example.xukaijun.avertallergy.activities.MainFragment;
import com.example.xukaijun.avertallergy.activities.SearchFragment;
import com.example.xukaijun.avertallergy.activities.ShistoryFragment;
import com.example.xukaijun.avertallergy.activities.SymptomFragment;

/*import com.example.xukaijun.avertallergy.activities.DieFragment;
import com.example.xukaijun.avertallergy.activities.MapFragment;
import com.example.xukaijun.avertallergy.activities.ReportFragment;
import com.example.xukaijun.avertallergy.activities.StepFragment;*/

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private final AppCompatActivity activity = MainActivity.this;
    private int id;
    private String codeid;
    private TextView textView_format;
    private TextInputEditText textInputEditTextBarcode;
    private NavigationView navigationView;
    private long exitTime = 0;
    private BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        Intent intent = getIntent();
        Bundle bundle=intent.getExtras();
        id=bundle.getInt("id");
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new MainFragment(),"mainfragment").commit();
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setLabelVisibilityMode(1);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.logocopy);
        toolbar.setOverflowIcon(drawable);
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Context getContext ()
    {
        return getApplicationContext();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //bottom navigation
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            int id = item.getItemId();
            Fragment nextFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    nextFragment = new MainFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, nextFragment, "mainfragment").commit();
                    return true;
                case R.id.navigation_allergens:
                    nextFragment = new EditFragment();
                    FragmentManager fragmentManager1 = getSupportFragmentManager();
                    fragmentManager1.beginTransaction().replace(R.id.content_frame, nextFragment).commit();
                    return true;
                case R.id.navigation_symptom:
                    nextFragment = new SymptomFragment();
                    FragmentManager fragmentManager2 = getSupportFragmentManager();
                    fragmentManager2.beginTransaction().replace(R.id.content_frame, nextFragment).commit();
                    return true;
                case R.id.navigation_search:
                    nextFragment = new SearchFragment();
                    FragmentManager fragmentManager3 = getSupportFragmentManager();
                    fragmentManager3.beginTransaction().replace(R.id.content_frame, nextFragment).commit();
                    return true;
                case R.id.navigation_list:
                    nextFragment = new ListFragment();
                    FragmentManager fragmentManager4 = getSupportFragmentManager();
                    fragmentManager4.beginTransaction().replace(R.id.content_frame, nextFragment).commit();
                    return true;
            }
            return false;
        }
    };

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment nextFragment = null;
        String flag = " ";
        if (id == R.id.nav_camera) {
            showBottomNavigationView(navigation);
            flag = "mainfragment";
            nextFragment = new MainFragment();
        } else if (id == R.id.nav_history) {
            hideBottomNavigationView(navigation);
            nextFragment = new HistoryFragment();
        } else if (id == R.id.nav_shistory) {
            hideBottomNavigationView(navigation);
            nextFragment = new ShistoryFragment();
        } else if (id == R.id.nav_help) {
            hideBottomNavigationView(navigation);
            nextFragment = new HelpFragment();
        }else if (id == R.id.nav_info) {
            hideBottomNavigationView(navigation);
            nextFragment = new AboutUsFragment();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame,
                nextFragment,flag).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout); drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //press twice to exit
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            //FragmentManager fragmentManager = getSupportFragmentManager();
            //fragmentManager.beginTransaction().replace(R.id.content_frame,
            //        new MainFragment()).commit();
            MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag("mainfragment");
            if (mainFragment == null) {
                Intent mainActivity = new Intent(getApplication(), MainActivity.class);
                mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putInt("id", id);
                mainActivity.putExtras(bundle);
                startActivity(mainActivity);
            }else
                {
                    Toast.makeText(getApplicationContext(), "Press again to exit application",
                            Toast.LENGTH_SHORT).show();
                    exitTime = System.currentTimeMillis();
                }
        } else {
            finish();
            System.exit(0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getContext(), LoginActivity.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideBottomNavigationView(BottomNavigationView view) {
        view.clearAnimation();
        view.animate().translationY(view.getHeight()).setDuration(300);
    }

    public void showBottomNavigationView(BottomNavigationView view) {
        view.clearAnimation();
        view.animate().translationY(0).setDuration(300);
    }
}
