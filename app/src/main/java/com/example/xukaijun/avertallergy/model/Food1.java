package com.example.xukaijun.avertallergy.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Food1 {
    public long code;
    public String image_small_url;
    public String image_url;
    public String ingredients_text;
    public int negative;
    public int positive;
    public String product_name;

    public Food1() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Food1(long code, String image_small_url,String image_url,String ingredients_text,int negative,int positive,String product_name) {
        this.code = code;
        this.image_small_url = image_small_url;
        this.image_url = image_url;
        this.ingredients_text = ingredients_text;
        this.negative = negative;
        this.positive = positive;
        this.product_name = product_name;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public void setImage_small_url(String image_small_url) {
        this.image_small_url = image_small_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setIngredients_text(String ingredients_text) {
        this.ingredients_text = ingredients_text;
    }

    public void setNegative(int negative) {
        this.negative = negative;
    }

    public void setPositive(int positive) {
        this.positive = positive;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", code);
        result.put("image_small_url", image_small_url);
        result.put("image_url", image_url);
        result.put("ingredients_text", ingredients_text);
        result.put("negative", negative);
        result.put("positive", positive);
        result.put("product_name", product_name);

        return result;
    }
}
