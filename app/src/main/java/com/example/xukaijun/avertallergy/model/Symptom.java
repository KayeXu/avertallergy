package com.example.xukaijun.avertallergy.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Symptom {

    @PrimaryKey(autoGenerate = true) public int sid;
    @ColumnInfo(name = "symptom_name")public String symptom_name;
    @ColumnInfo(name = "sdate")public String sdate;
    @ColumnInfo(name = "comment")public String comment;
    @ColumnInfo(name = "level")public String level;

    public int getId() {
        return sid;
    }

    public void setId(int id) {
        this.sid = id;
    }

    public String getSymptom_name() {
        return symptom_name;
    }

    public void setSymptom_name(String symptom_name) {
        this.symptom_name = symptom_name;
    }

    public String getSdate(){return sdate;}

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getComment(){return comment;}

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

}