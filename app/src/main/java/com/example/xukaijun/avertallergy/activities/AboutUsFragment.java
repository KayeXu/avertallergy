package com.example.xukaijun.avertallergy.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.xukaijun.avertallergy.R;

public class AboutUsFragment extends Fragment {

    View vMain;
    private Button appCompatButtonweb;
    //initial the view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        vMain = inflater.inflate(R.layout.fragment_aboutus, container, false);
        getActivity().setTitle("About us");
        appCompatButtonweb = vMain.findViewById(R.id.appCompatButtonweb);
        appCompatButtonweb.setOnClickListener(new View.OnClickListener() { //including onClick() method
            public void onClick(View v) {
                Intent intent= new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse("http://avertallergy.tk.s3-website.us-east-2.amazonaws.com");
                intent.setData(content_url);
                startActivity(intent);
            }
        });
        return vMain;
    }
}
