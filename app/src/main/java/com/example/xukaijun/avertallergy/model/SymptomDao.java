package com.example.xukaijun.avertallergy.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface SymptomDao
{
    @Query("SELECT * FROM symptom")
    List<Symptom> getAll();
    @Query("SELECT * FROM symptom WHERE sdate LIKE :sdate LIMIT 1")
    Symptom findBySdate(String sdate);
    @Query("SELECT * FROM symptom WHERE sid = :sid LIMIT 1")
    Symptom findByID(int sid);
    @Insert
    void insertAll(Symptom... symptoms);
    @Insert
    long insert(Symptom symptom);
    @Delete
    void delete(Symptom symptom);
    @Update(onConflict = REPLACE)
    public void updateSymptoms(Symptom... symptoms);
    @Query("DELETE FROM Symptom")
    void deleteAll();
    @Query("DELETE FROM symptom WHERE sdate = :sdate")
    void deleteBySdate(String sdate);
}