package com.example.xukaijun.avertallergy.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Symptom.class}, version = 2, exportSchema = false) public abstract class SymptomDatabase extends RoomDatabase {
    public abstract SymptomDao symptomDao();
    private static volatile SymptomDatabase INSTANCE;
    static SymptomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SymptomDatabase.class) {
                if (INSTANCE == null) { INSTANCE =
                        Room.databaseBuilder(context.getApplicationContext(), SymptomDatabase.class, "symptom_database")
                                .build();
                } }
        }
        return INSTANCE; }
}